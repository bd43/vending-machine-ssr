This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app). 
It contains a vending machine-like mechanism: the user can add money (using certain denominations) to update his/her current ballance and must use a virtual keypad to select the product.

## Vending Machine app

![Vending Machine app](https://i.imgur.com/MG16n3Y.png "Vending Machine App")

- ReactJS
- NextJs (SSR + automatic code-splitting)
- ExpressJS for API management
- MongoDB with Mongoose (make sure to start the MongoDB service **before** starting the development server)


Nextjs will serve each file in the `pages` folder under a pathname matching the filename. By design, the project only has one page (`index`).

It uses SSR by default and includes an Express server to better handle the API calls.

First, install the npm dependencies:
```bash
npm install
# or
yarn
```
After that, you can start the express server:
```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:8000](http://localhost:8000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.
The project is also configured to run ESlint and TSLint on compilation.

