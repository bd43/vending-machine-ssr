import * as React from 'react';
import { connect } from 'react-redux';
import {
  loadProducts, updateBalance, updateSelection, buyProduct, updateSuccessStatus,
} from '../src/saga/actions';
import { State, IProduct, PageProps } from '~interfaces/all';
import BasePage from './_base';
import VendingMachine from '~components/VendingMachine';
import { Dispatch } from 'redux';

interface StoreProps {
  products: IProduct[];
  balance: number;
  selection: string;
  success: boolean;
  addMoney: (currency: number) => void;
  updateSelection: (selection: string) => void;
  buyProduct: (productID: string) => void;
  updateSuccessStatus: (status: boolean) => void;
}

type HomePageProps = PageProps & StoreProps;

class HomePage extends BasePage<HomePageProps, any> {
  static async getInitialProps(ctx: any) {
    const initialProps = await super.getInitialProps(ctx);
    await super.makeRequest(ctx, loadProducts);

    return {
      ...initialProps
    };
  }

  componentDidMount() {
    console.log('this.props.products', this.props.products);
  }

  render() {
    return <VendingMachine {...this.props}/>;
  }
}

const mapStateToProps = (state: State) => ({
  products: state.products,
  balance: state.balance,
  selection: state.selection,
  success: state.success
});

const mapDispatchToProps = (dispatch: Dispatch) => ({
  addMoney: (c: number) => dispatch(updateBalance(c)),
  updateSelection: (d: string) => dispatch(updateSelection(d)),
  buyProduct: (productID: string) => dispatch(buyProduct(productID)),
  updateSuccessStatus: (status: boolean) => dispatch(updateSuccessStatus(status))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
