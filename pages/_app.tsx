import React from 'react';
import App from 'next/app';
import '../src/styles/style.scss';
import { Provider } from 'react-redux';
import withRedux from 'next-redux-wrapper';
import withReduxSaga from 'next-redux-saga';
import createStore from '../src/redux/store';
import { IApp } from '~interfaces/all';
import storeServiceInstance from '~utils/storeServiceInstance';

class MyApp extends App<IApp.IProps, null> {
  static async getInitialProps(props: any) {
    const { Component, ctx } = props;

    let pageProps: any = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return {
      pageProps,
      query: ctx.query
    };
  }

  constructor(props: any) {
    super(props);
    this.state = {
      isResponsive: false
    };
  }

  render() {
    const { Component, pageProps, store, query } = this.props;
    storeServiceInstance.setStore(store);

    return (
        <Provider store={store}>
          <React.Fragment>
            <Component
              {...pageProps}
              rootQuery={query}
            />
          </React.Fragment>
        </Provider>
    );
  }
}

export default withRedux(createStore)(withReduxSaga(MyApp));
