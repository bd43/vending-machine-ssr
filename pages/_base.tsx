import * as React from 'react';
import { NextPageContext } from 'next';

type BasePageContext = NextPageContext & {
  isServer: boolean;
};

class BasePage<P, S> extends React.Component<P, S> {
  static async getInitialProps(ctx: BasePageContext) {
    /* const userAgent =
      (ctx.req ? ctx.req.headers['user-agent'] : navigator.userAgent) || ''; */
    return {
      query: ctx.query,
      isServer: ctx.isServer || false
    };
  }

  static async makeRequest(ctx: any, action: any, params?: {}) {
    await new Promise(resolve => {
      ctx.store.dispatch(action(params, ctx));
      const unsubscribe = ctx.store.subscribe(() => {
        unsubscribe();
        return resolve();
      });
    });
  }
}
export default BasePage;
