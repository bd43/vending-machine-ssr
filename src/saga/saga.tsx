import { all, takeEvery } from 'redux-saga/effects';
import { ActionTypes } from './actionTypes';
import { loadProductsSaga, buyProductSaga } from './sagas/productsSaga';

function* rootSaga() {
  yield all([
    takeEvery(ActionTypes.LOAD_PRODUCTS_REQUEST, loadProductsSaga),
    takeEvery(ActionTypes.BUY_PRODUCT_REQUEST, buyProductSaga)
  ]);
}

export default rootSaga;
