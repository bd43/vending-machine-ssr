import { ActionTypes } from './actionTypes';
import { IProduct } from '~interfaces/all';

export const loadProducts = () => ({
  type: ActionTypes.LOAD_PRODUCTS_REQUEST
});

export const loadProductsSuccess = (data: IProduct[]) => ({
  type: ActionTypes.LOAD_PRODUCTS_SUCCESS,
  payload: data
});

export const loadProductsFailure = (data: any) => ({
  type: ActionTypes.LOAD_PRODUCTS_FAILURE,
  payload: data
});

export const updateProduct = (data: IProduct) => ({
  type: ActionTypes.UPDATE_PRODUCT,
  payload: data
});

export const updateBalance = (data: number) => ({
  type: ActionTypes.UPDATE_BALANCE,
  payload: +(data).toFixed(12)
});

export const updateSelection = (data: string) => ({
  type: ActionTypes.UPDATE_SELECTION,
  payload: data
});

export const resetSelection = () => ({
  type: ActionTypes.RESET_SELECTION
});

export const buyProduct = (data: string) => ({
  type: ActionTypes.BUY_PRODUCT_REQUEST,
  payload: data
});

export const buyProductSuccess = (data: IProduct[]) => ({
  type: ActionTypes.BUY_PRODUCT_SUCCESS,
  payload: data
});

export const buyProductFailure = (data: any) => ({
  type: ActionTypes.BUY_PRODUCT_FAILURE,
  payload: data
});

export const updateSuccessStatus = (data: boolean) => ({
  type: ActionTypes.UPDATE_SUCCESS_STATUS,
  payload: data
})