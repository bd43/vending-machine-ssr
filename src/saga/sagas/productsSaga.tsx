import { call, put, select } from 'redux-saga/effects';
import { makeAPICall } from '~utils/api';
import { updateBalance, loadProductsSuccess, updateProduct, updateSuccessStatus } from '~saga/actions';
import { AnyAction } from 'redux';
import { State } from '~interfaces/all';

export function* loadProductsSaga() {
  try {
    const response = yield call(makeAPICall, '/products');
    yield put(loadProductsSuccess(response));
  } catch (error) {
    console.error('Failed to fetch products');
  }
}

export function* buyProductSaga(data: AnyAction) {
  try {
    const currentBalance = yield select((state: State) => state.balance); 
    const response = yield call(makeAPICall, '/products', 'post', {id: data.payload});
    yield put(updateBalance(currentBalance - response.price));
    yield put(updateProduct(response));
    yield put(updateSuccessStatus(true));
  } catch (error) {
    console.error('Failed to fetch products', error);
  }
}
