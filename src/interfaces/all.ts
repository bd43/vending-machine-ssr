export interface IConfig {
  [key: string]: {
    API_URL: string;
    FE_URL: string;
  };
}

export declare namespace IApp {
  export interface IProps {
    store: any;
    Component: any;
    pageProps: any;
    query?: any;
  }
}

export interface PageProps {
  query: any;
  isServer: boolean;
}

export interface IProduct {
  _id: string;
  name: string;
  quantity: number;
  price: number;
  slug: string;
  internal_id: string;
  created_at: Date;
  updated_at: Date;
}

export interface IButton {
  readonly isActive?: boolean;
  fullWidth?: boolean;
  borderStyle: 'solid' | 'dashed';
  color?: string;
  children: any;
  onClick(e: React.MouseEvent<HTMLElement>): void
}

export interface State {
  products: IProduct[];
  balance: number;
  selection: string;
  success: boolean;
}
