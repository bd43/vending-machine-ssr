export class StoreService {
  private store: any;

  public setStore = (store: any): void => {
    this.store = store;
  };

  public getStore = (): any => {
    return this.store;
  };
}

let storeServiceInstance = new StoreService();

export default storeServiceInstance;
