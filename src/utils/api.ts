import axios, { AxiosRequestConfig } from 'axios';
import { axiosInstanceConfig } from '../config/constants';

const axiosInstance = axios.create(axiosInstanceConfig);

export const makeAPICall = (
    route: string,
    type?: 'get' | 'post' | 'put' | 'delete',
    data?: any,
    config?: AxiosRequestConfig
) => {
    if (type === 'post') {
        return postAPICall(route, data, config);
    } else if (type === 'put') {
        return putAPICall(route, data, config);
    } else if (type === 'delete') {
        return deleteAPICall(route, data, config);
    } else {
        return getAPICall(route, config);
    }
};

export const getAPICall = (route: string, config?: AxiosRequestConfig) => {
    return axiosInstance
        .get(route, config)
        .then(response => response.data)
        .catch(err => {
            throw err;
        });
};

export const postAPICall = (
    route: string,
    data?: any,
    config?: AxiosRequestConfig
) => {
    return axiosInstance
        .post(route, data, config)
        .then(response => response.data)
        .catch(err => {
            throw err;
        });
};

export const putAPICall = (
    route: string,
    data?: any,
    config?: AxiosRequestConfig
) => {
    return axiosInstance
        .put(route, data, config)
        .then(response => response.data)
        .catch(err => {
            throw err;
        });
};

export const deleteAPICall = (
    route: string,
    data?: any,
    config?: AxiosRequestConfig
) => {
    return (
        axiosInstance
            // @ts-ignore
            .delete(route, { data, headers: { ...config.headers } })
            .then(response => response.data)
            .catch(err => {
                throw err;
            })
    );
};
