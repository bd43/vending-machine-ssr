const chunk = (arr: any, size: number) => {
    let res = [];
    while (arr.length) {
        res.push(arr.splice(0, size));
    }
    return res;
}

export default chunk;