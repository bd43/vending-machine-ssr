import { AxiosRequestConfig } from 'axios';
import config from './config';

export const axiosInstanceConfig: AxiosRequestConfig = {
  baseURL: config.API_URL,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
};

export const VENDING_MACHINE_ROWS: string[] = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase().split('');
export const VENDING_MACHINE_NO_ROWS: number = 5;
export const VENDING_MACHINE_CURRENCY: string = '€';
export const VENDING_MACHINE_DENOMINATIONS = [0.1, 0.5, 1, 2];