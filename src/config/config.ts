import getConfig from 'next/config';
import { IConfig } from '~interfaces/all';

const { publicRuntimeConfig } = getConfig();
const env = publicRuntimeConfig.ENV || 'local';

const config: IConfig = {
  local: {
    API_URL: 'http://localhost:8000/api/',
    FE_URL: 'http://localhost:8000'
  },
  production: {
    API_URL: '/',
    FE_URL: '/'
  }
};

export default config[env];
