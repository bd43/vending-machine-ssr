import initialState from '../initialState';
import { AnyAction } from 'redux';
import { ActionTypes } from '../../saga/actionTypes';
import { IProduct } from '~interfaces/all';

const products = (state = initialState.products, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.UPDATE_PRODUCT:
      const index = state.findIndex((product: IProduct) => product._id === action.payload._id);
      return [
        ...state.slice(0, index),
        action.payload,
        ...state.slice(index + 1)
      ];
    case ActionTypes.LOAD_PRODUCTS_SUCCESS:
      return [ ...state, ...action.payload ];
    default:
      return state;
  }
};

export default products;
