import initialState from '../initialState';
import { AnyAction } from 'redux';
import { ActionTypes } from '../../saga/actionTypes';

const balance = (state = initialState.balance, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.UPDATE_BALANCE:
      return action.payload;
    default:
      return state;
  }
};

export default balance;
