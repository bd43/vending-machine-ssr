import initialState from '../initialState';
import { AnyAction } from 'redux';
import { ActionTypes } from '../../saga/actionTypes';

const success = (state = initialState.success, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.UPDATE_SUCCESS_STATUS:
      return action.payload;
    default:
      return state;
  }
};

export default success;
