import initialState from '../initialState';
import { AnyAction } from 'redux';
import { ActionTypes } from '../../saga/actionTypes';

const selection = (state = initialState.selection, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.UPDATE_SELECTION:
      return action.payload;
    case ActionTypes.RESET_SELECTION:
      return '';
    default:
      return state;
  }
};

export default selection;
