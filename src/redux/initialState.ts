import { State } from '~interfaces/all';

const initialState: State = {
  products: [],
  balance: 0,
  selection: '',
  success: false
};

export default initialState;
