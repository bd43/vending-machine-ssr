import { applyMiddleware, combineReducers, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
import initialState from './initialState';
import rootSaga from '../saga/saga';
import products from './reducers/products';
import balance from './reducers/balance';
import selection from './reducers/selection';
import success from './reducers/success';


const rootReducer = combineReducers({
 products,
 balance,
 selection,
 success
});

const bindMiddleware = (middleware: any) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

function configureStore(state = initialState) {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    rootReducer,
    state as any,
    bindMiddleware([sagaMiddleware])
  );

  // @ts-ignore
  store.sagaTask = sagaMiddleware.run(rootSaga);

  return store;
}

export default configureStore;
