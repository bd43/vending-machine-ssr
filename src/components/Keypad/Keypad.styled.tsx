import styled from 'styled-components';
import { Button, Row } from 'reactstrap';

export const KeypadSelection = styled.div`
    background-color: #d63031;
    max-width: 100%;
    text-align: center;
    color: #ffffff;
    text-transform: uppercase;
    font-size: 3rem;
    line-height: 5rem;
    min-height: 5rem;
    border-radius: 0.25rem;
    height: 100%;
`;

export const Key = styled(Button)`
    &&& {
        width: 2.5rem;
        height: 2.5rem;
        border-radius: 50%;
        margin-right: 0.5rem;
        transition: all 500ms cubic-bezier(0.190, 1.000, 0.220, 1.000);
        font-size: 1.2rem;
        &:last-child {
            margin-right: 0
        }
    }
`;

export const KeypadRow = styled(Row)`{
    &&& {
        margin-bottom: 1rem;
        opacity: ${props => props.disabled ? '0.7' : '1'};
        pointer-events: ${props => props.disabled ? 'click-trough' : 'auto'};
    }
}`;
