import * as React from 'react';
import {
  VENDING_MACHINE_ROWS,
  VENDING_MACHINE_NO_ROWS
} from '~config/constants';
import { Row, Col } from 'reactstrap';
import { KeypadSelection, Key, KeypadRow } from './Keypad.styled';

interface KeypadProps {
  disabled: boolean;
  selection: string;
  handleKeypadSelection: (selection: string) => void;
  quantityError: string;
}
interface KeypadState {
  row: string;
  column: string;
}

class Keypad extends React.PureComponent<KeypadProps, KeypadState> {
  constructor(props: KeypadProps) {
    super(props);
    this.state = {
      row: '',
      column: ''
    };
  }

  componentDidUpdate(prevProps: KeypadProps) {
    if (
      prevProps.disabled !== this.props.disabled ||
      (prevProps.quantityError !== this.props.quantityError &&
        this.props.quantityError.length > 0)
    ) {
      this.setState({
        row: '',
        column: ''
      });
    }
  }

  private handleClick = (selection: string): void => {
    const { row, column } = this.state;
    this.setState(
      {
        row: !/^\d+$/.test(selection) ? selection : row,
        column: /^\d+$/.test(selection) ? selection : column
      },
      () => {
        this.props.handleKeypadSelection(this.state.row + this.state.column);
      }
    );
  };

  render() {
    const { row, column } = this.state;
    const { disabled } = this.props;
    return (
      <React.Fragment>
        <Row>
          <Col xs={6} lg={12}>
            <KeypadRow>
              <Col xs={6} lg={12} className={'text-center'}>
                {VENDING_MACHINE_ROWS.slice(0, VENDING_MACHINE_NO_ROWS).map(
                  (letter: string) => (
                    <Key
                      outline
                      color={'warning'}
                      onClick={() => this.handleClick(letter)}
                      key={`key-${letter}`}
                      disabled={disabled}
                    >
                      {letter}
                    </Key>
                  )
                )}
              </Col>
            </KeypadRow>
            <KeypadRow>
              <Col  className={'text-center'}>
                {[...Array(5).keys()].map((key: number) => (
                  <Key
                    outline
                    color={'warning'}
                    onClick={() => this.handleClick((key + 1).toString())}
                    key={`key-${key + 1}`}
                    disabled={!row || disabled}
                  >
                    {key + 1}
                  </Key>
                ))}
              </Col>
            </KeypadRow>
          </Col>
          <Col xs={6} lg={12}>
            <KeypadSelection>
              {row}
              {column}
            </KeypadSelection>
          </Col>
        </Row>
        <br />
      </React.Fragment>
    );
  }
}

export default Keypad;
