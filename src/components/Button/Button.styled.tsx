import styled from 'styled-components';
import { IButton } from '~interfaces/all';

export const ButtonBase = styled.button<IButton>`
    outline: none;
    border: 0.25rem ${props => props.borderStyle} ${props => props.color ? props.color : '#ffffff'};
    width: ${props => props.fullWidth ? '100%' : 'auto'};
    color: ${props => props.color ? props.color : '#ffffff'};
    margin-bottom: 0.5rem;
    background-color: transparent;
    font-size: 1.5rem;
    cursor: pointer;
`;
