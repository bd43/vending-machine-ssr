import * as React from 'react';
import { ButtonBase } from './Button.styled';
import { IButton } from '~interfaces/all';

const Button = (props: IButton) => {
    const {children, ...rest} = props;
    return <ButtonBase {...rest}>{children}</ButtonBase>;
};

export default Button;
