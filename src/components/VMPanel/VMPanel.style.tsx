import styled from 'styled-components';

export const VMPanelWrapper = styled.div`
    background-color: #e74c3c;
    padding: 2rem 0;
    color: #ffffff;
    position: relative;
    height: 100vh;
`;
