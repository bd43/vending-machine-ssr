import * as React from 'react';
import { VMPanelWrapper } from './VMPanel.style';
import { Container, Row, Col, Alert, Button } from 'reactstrap';
import Wallet from '~components/Wallet';
import Keypad from '~components/Keypad';

interface VMPanelProps {
  handleMoney: (currency: number) => void;
  handleKeypadSelection: (selection: string) => void;
  balance: number;
  selection: string;
  success: boolean;
  errors: {
    [key: string]: string;
  };
}

const VMPanel = (props: VMPanelProps) => {
  const { balance: balanceError, quantity: quantityError } = props.errors;
  return (
    <VMPanelWrapper>
      <Container>
        <Row>
          <Col xs={12}>
            <h2 className={'text-center'}>Insert cash</h2>
          </Col>
          <Col xs={12}>
            <Row>
              <Col xs={12}>
                <Wallet
                  balance={props.balance}
                  handleMoney={props.handleMoney}
                />
              </Col>
              <Col xs={12}>
                <Button
                  color='success'
                  size='lg'
                  block
                  onClick={() => props.handleMoney(-1 * props.balance)}
                  disabled={props.balance === 0}
                >
                  Restore change
                </Button>
              </Col>
            </Row>
            {balanceError && balanceError.length > 0 && (
              <Alert color={'warning'}>{balanceError}</Alert>
            )}
            <br/>
          </Col>
        </Row>
        <Row>
          <Col xs={12}>
            <h2 className={'text-center'}>Pick the product</h2>
          </Col>
          <Col xs={12}>
            <Keypad
              selection={props.selection}
              handleKeypadSelection={props.handleKeypadSelection}
              disabled={props.balance === 0 || balanceError !== '' || props.success}
              quantityError={quantityError}
            />
            {quantityError && quantityError.length > 0 && (
              <Alert color={'warning'}>{quantityError}</Alert>
            )}
          </Col>
        </Row>
      </Container>
    </VMPanelWrapper>
  );
};

export default VMPanel;
