import styled from 'styled-components';

export const SlotWrapper = styled.div`
    padding: 0.25rem 0;
    position: relative;
    z-index: 5;
`;

export const SlotLabel = styled.div`
    font-size: 1.2em;
    line-height: 1.2em;
    font-weight: bold;
    background-color: #000000;
    color: #ffffff;
    padding: 0 0.4rem;
    display: block;
    position: absolute;
    bottom: -1.5rem;
    left: 50%;
    transform: translateX(-50%);
    z-index: 4;
`;