import * as React from 'react';
import { IProduct } from '~interfaces/all';
import Product from '~components/Product/Product';
import { SlotWrapper, SlotLabel } from './Slot.style';

interface SlotProps {
    slot: string;
    product: IProduct;
}

const Slot = (props: SlotProps) => (
    <SlotWrapper>
        <Product {...props.product} />
        <SlotLabel>{props.slot}</SlotLabel>
    </SlotWrapper>

);

export default Slot;