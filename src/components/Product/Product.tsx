import * as React from 'react';
import { ProductWrapper } from './Product.style';
import { IProduct } from '~interfaces/all';
import { VENDING_MACHINE_CURRENCY } from '~config/constants';

function Product(props: IProduct) {
  return (
    <ProductWrapper outOfStock={props.quantity === 0}>
      <span className={'product-quantity'}>{props.quantity}</span>
      <img
        src={`/images/products/${props.slug}.png`}
        alt={props.name} 
        title={props.name}
        className={`product-image`}
      />
      <p className={'product-price'}>{props.price} {VENDING_MACHINE_CURRENCY}</p>
    </ProductWrapper>
  );
}

export default Product;
