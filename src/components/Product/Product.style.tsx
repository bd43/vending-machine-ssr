import styled from 'styled-components';

interface ProductWrapperProps{
  outOfStock: boolean;
}

export const ProductWrapper = styled.div<ProductWrapperProps>`
  position: relative;
  border-radius: 2rem;
  text-align: center;
  z-index: 1;
  opacity: ${props => props.outOfStock ? '0.7' : 1};
  img {
    max-width: 9rem;
    width: 100%;
    margin: 0 auto;
    position: relative;
    z-index: 1;
  }
  .product-name {
    font-size: 1rem;
    text-align: center;
    white-space: nowrap;
    overflow: hidden;
    max-width: 10rem;
    text-overflow: ellipsis;
    margin: 0;
  }
  .product-price {
    font-size: 1rem;
    font-weight: bold;
    text-align: center;
    margin: 0;
  }
  .product-quantity {
    display: block;
    position: absolute;
    width: 1rem;
    height: 1rem;
    top: 0.5rem;
    right: 0.5rem;
    background-color: #e74c3c;
    color: #ffffff;
    font-size 0.8em;
    line-height: 1rem;
    border-radius: 50%;
    text-align: center;
    z-index: 4;
  }
`;
