import styled from 'styled-components';
import { Row, Container } from 'reactstrap';

export const VMWrapper = styled.div`
    background-color: #ffffff;
    max-width: 100%;
    height: 100vh;
`;

export const VMUnit = styled(Container)`
    position: relative;
    max-width: 30rem;
    margin: 5rem auto 5rem;
    border-radius: 1rem;
    border: 1.5rem solid #22a6b3;
    border-right: 5rem solid #22a6b3;
    padding-bottom: 1.5rem;
    /* Fake keypad */
    &:before {
        content: '';
        display: block;
        position: absolute;
        background-color: #2c3e50;
        border-radius: 0.25rem;
        width: 2.5rem;
        height: 2rem;
        right: -3.75rem;
        top: 45%;
        z-index: 5;
    }
    &:after { 
        content: '';
        display: block;
        position: absolute;
        background-color: #bdc3c7;
        border-radius: 0.25rem;
        width: 3rem;
        height: 4.5rem;
        right: -4rem;
        top: 50%;
        z-index: 4;
        transform: translateY(-50%);
    }
`;

export const VMRow = styled(Row)`
    padding-bottom: 0.5rem;
    margin-bottom: 1.25rem;
    position: relative;
    z-index: 3;
    &:after {
        content: '';
        display: block;
        background-color: #dddddd;
        position: absolute;
        bottom: -0.5rem;
        height: 0.5rem;
        width: 100%;
        z-index: 3;
        -webkit-box-shadow: 0px 7px 12px 0px rgba(0,0,0,0.39);
        -moz-box-shadow: 0px 7px 12px 0px rgba(0,0,0,0.39);
        box-shadow: 0px 7px 12px 0px rgba(0,0,0,0.39);
    }
`;
