import * as React from 'react';
import { VMWrapper, VMRow, VMUnit } from './VendingMachine.styled';
import { IProduct } from '~interfaces/all';
import { Col, Row, Container } from 'reactstrap';
import chunk from '~utils/chunk';
import Slot from '~components/Slot/Slot';
import VMPanel from '~components/VMPanel';
import {
  VENDING_MACHINE_NO_ROWS,
  VENDING_MACHINE_ROWS
} from '~config/constants';
import VMModal from '~components/Modal/Modal';

interface VendingMachineProps {
  products: IProduct[];
  balance: number;
  selection: string;
  success: boolean;
  addMoney: (currency: number) => void;
  updateSelection: (selection: string) => void;
  buyProduct: (productID: string) => void;
  updateSuccessStatus: (status: boolean) => void;
}

interface VendingMachineState {
  productMap: {
    [key: string]: {
      productID: string;
      quantity: number;
      price: number;
    };
  };
  errors: {
    [key: string]: string;
  };
}

class VendingMachine extends React.Component<
  VendingMachineProps,
  VendingMachineState
> {
  constructor(props: VendingMachineProps) {
    super(props);
    this.state = {
      productMap: {},
      errors: {
        balance: '',
        quantity: ''
      }
    };
  }

  componentDidMount() {
    this.generateProductMap();
  }

  componentDidUpdate(prevProps: VendingMachineProps) {
    if (
      JSON.stringify(prevProps.products) !== JSON.stringify(this.props.products)
    ) {
      this.generateProductMap();
      this.resetErrors();
    }
    if (prevProps.balance != this.props.balance) {
      this.resetErrors();
    }
  }

  private generateProductMap = () => {
    this.setState({
      productMap: chunk(
        [...this.props.products],
        VENDING_MACHINE_NO_ROWS
      ).reduce((acc: any, currentChunk: IProduct[], currentRow: number) => {
        currentChunk.forEach((product: IProduct, currentColumn: number) => {
          acc[`${VENDING_MACHINE_ROWS[currentRow]}${currentColumn + 1}`] = {
            productID: product._id,
            price: product.price,
            quantity: product.quantity
          };
        });
        return acc;
      }, {})
    });
  };

  private resetErrors = () => {
    this.setState({
      errors: {
        balance: '',
        quantity: ''
      }
    });
  };

  private handleMoney = (currency: number) => {
    this.props.addMoney(this.props.balance + currency);
  };

  private handleKeypadSelection = (selection: string) => {
    this.props.updateSelection(selection);
    this.resetErrors();
    if (this.state.productMap[selection]) {
      if (this.props.balance < this.state.productMap[selection].price) {
        this.setState({
          errors: {
            ...this.state.errors,
            balance: 'Insufficient funds'
          }
        });
        return;
      }
      if (this.state.productMap[selection].quantity === 0) {
        this.setState({
          errors: {
            ...this.state.errors,
            quantity: 'The product is out of stock. Pick another one.'
          }
        });
        return;
      }
      this.props.buyProduct(this.state.productMap[selection].productID);
    }
  };

  private handleToggle = (status: boolean) => {
    this.props.updateSuccessStatus(status);
  };

  render() {
    const productChunks = chunk(
      [...this.props.products],
      VENDING_MACHINE_NO_ROWS
    );
    return (
      <React.Fragment>
        <Container fluid>
          <VMWrapper>
            <Row>
              <Col xs={8}>
                <VMUnit>
                  {productChunks.map((chunk: IProduct[], rowIndex: number) => (
                    <VMRow key={`row-${VENDING_MACHINE_ROWS[rowIndex]}`}>
                      {chunk.map((product: IProduct, columnIndex: number) => (
                        <Col key={`col-${columnIndex}`}>
                          <Slot
                            slot={`${
                              VENDING_MACHINE_ROWS[rowIndex]
                            }${columnIndex + 1}`}
                            product={product}
                          />
                        </Col>
                      ))}
                    </VMRow>
                  ))}
                </VMUnit>
              </Col>
              <Col xs={4}>
                <VMPanel
                  handleMoney={this.handleMoney}
                  handleKeypadSelection={this.handleKeypadSelection}
                  balance={this.props.balance}
                  selection={this.props.selection}
                  errors={this.state.errors}
                  success={this.props.success}
                />
              </Col>
            </Row>
          </VMWrapper>
        </Container>
        <VMModal handleToggle={this.handleToggle} status={this.props.success}>
          <h2 style={{ textAlign: 'center' }}>Transaction successful!</h2>
        </VMModal>
      </React.Fragment>
    );
  }
}

export default VendingMachine;
