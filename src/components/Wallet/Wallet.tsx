import * as React from 'react';
import {
  Row,
  Col,
  Button,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input
} from 'reactstrap';
import { VENDING_MACHINE_CURRENCY, VENDING_MACHINE_DENOMINATIONS } from '~config/constants';

interface WalletProps {
  balance: number;
  handleMoney: (currency: number) => void;
}

const Wallet = (props: WalletProps) => {
  return (
    <React.Fragment>
      <Row style={{ marginBottom: '1rem' }}>
        <Col xs={12}>
          <Row>
            {VENDING_MACHINE_DENOMINATIONS.map((val: number) => (
              <Col>
                <Button
                  color={'warning'}
                  onClick={() => props.handleMoney(val)}
                  key={`cash-${val}`}
                  block
                >
                  {val} {VENDING_MACHINE_CURRENCY}
                </Button>
              </Col>
            ))}
            <br/>
          </Row>
        </Col>
        <Col xs={12}>
          <br/>
          <InputGroup size={'md'}>
            <InputGroupAddon addonType='prepend'>
              <InputGroupText>{VENDING_MACHINE_CURRENCY}</InputGroupText>
            </InputGroupAddon>
            <Input
              placeholder='Insert coins'
              value={props.balance || 'Insert coins'}
              readOnly
            />
          </InputGroup>
        </Col>
      </Row>
    </React.Fragment>
  );
};

export default Wallet;
