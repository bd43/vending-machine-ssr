import * as React from 'react';
import { Modal, ModalBody, Button, ModalFooter } from 'reactstrap';

interface ModalProps {
    status: boolean;
    handleToggle: (status: boolean) => void;
    children?: any;
}

const VMModal = (props: ModalProps) => {
    return (
        <div>
            <Modal isOpen={props.status} toggle={() => props.handleToggle(false)}>
                <ModalBody>
                    {props.children}
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={() => props.handleToggle(false)}>Close</Button>
                </ModalFooter>
            </Modal>
        </div>
    );
}

export default VMModal;