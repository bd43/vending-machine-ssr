const withPlugins = require('next-compose-plugins');
const withBundleAnalyzer = require('@zeit/next-bundle-analyzer');
const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withTM = require('next-transpile-modules');
const withImages = require('next-images');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const path = require('path');

const NEXT_CONFIG = {
  distDir: '_build',
  generateBuildId: async () => {
    // Next.js generates a hash constant at build time (BUILD_ID?)
    // This can cause problems in multi-server deployments
    // when `next build` is ran on every server
    if (process.env.I_HAZ_BUILD_ID) {
      // @TODO: get latest Git commit hash id ?
      // return process.env.I_HAZ_BUILD_ID; // hash?
    }

    return null; // fallback to random generated
  },
  publicRuntimeConfig: {
    // Will be available on both server and client
    ENV: process.env.ENV
  },
  webpack(config, options) {
    // Do not run type checking twice:
    if (options.isServer) {
      config.plugins.push(
        new ForkTsCheckerWebpackPlugin({
          tslint: path.resolve(__dirname, 'tslint.json'),
          tsconfig: path.resolve(__dirname, 'tsconfig.json')
        })
      );
    }

    if (process.env.ENV !== 'production') {
      config.devtool = 'source-map';
      config.optimization.minimize = false;
    }

    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]'
        }
      }
    });

    return config;
  }
};

module.exports = withPlugins(
  [
    withImages,
    withCSS,
    [
      withSass,
      {
        sassOptions: {
          includePaths: ['./src/styles']
        }
      }
    ],
    withBundleAnalyzer,
    [withTM, { transpileModules: [] }]
  ],
  NEXT_CONFIG
);
