
// maximum number of products per slot
const MAX_PER_SLOT = 10;

const products = [{
    name: 'Coca-Cola 0.5L',
    shortName: 'Coca-Cola',
    price: 3.25
}, {
    name: 'Coca-Cola Zero 0.5L',
    shortName: 'Coca-Cola Zero',
    price: 2.89
},
{
    name: 'Fanta Portocale 0.5L',
    shortName: 'Fanta Portocale',
    price: 2.85
},
{
    name: 'Sprite 0.5L',
    shortName: 'Sprite',
    price: 3.10
}, {
    name: 'Baton de ciocolata Snickers 50g',
    shortName: 'Snickers',
    price: 1.50
},
{
    name: 'Batoane de ciocolata si caramel Twix',
    shortName: 'Twix',
    price: 1.75
},
{
    name: 'Napolitana glazurata cu ciocolata lapte KitKat',
    shortName: 'KitKat',
    price: 2.15
},
{
    name: 'Biscuiti crema cacao Biskrem',
    shortName: 'Biskrem',
    price: 3.10
},
{
    name: 'Covrigei cu susan alb si negru Alka Toortitzi 180g',
    shortName: 'Toortitzi susan',
    price: 3.99
},
{
    name: 'Covrigei cu sare Alka Toortitzi 80g',
    shortName: 'Toortitzi sare',
    price: 2.55
},
{
    name: 'Snack cu pizza Toortitzi 180g',
    shortName: 'Toortitzi pizza',
    price: 3.99
},
{
    name: 'Covrigei cu mix de seminte Alka Toortitzi 180g',
    shortName: 'Toortitzi seminte',
    price: 3.75
},
{
    name: 'Cappy Nectar Portocale 0.5L',
    shortName: 'Cappy nectar portocale',
    price: 4.88
},
{
    name: 'Suc de piersici Cappy 0.33L',
    shortName: 'Cappy nectar piersici',
    price: 2.41,
},
{
    name: 'Cappy Nectar Pere 1L PET',
    shortName: 'Cappy nectar pere',
    price: 5.09
},
{
    name: 'Apa minerala naturala necarbogazoasa pet Dorna 0.5l',
    shortName: 'Dorna necarbogazoasa',
    price: 1.89
},
{
    name: 'Apa minerala naturala carbogazoasa pet Dorna 0.5l',
    shortName: 'Dorna carbogazoasa',
    price: 1.89
},
{
    name: 'Bautura racoritoare necarbogazoasa fortificata cu magneziu 0.6l Vitamin Aqua',
    shortName: 'Vitamin aqua balance',
    price: 5.41
},
{
    name: 'Bautura racoritoare necarbogazoasa fortificata cu vitamina B+ 0.6l Vitamin Aqua',
    shortName: 'Vitamin aqua focus',
    price: 5.23
},
{
    name: 'Bautura racoritoare necarbogazoasa cu aroma de ananas si fructul pasiunii 0.6l Vitamin Aqua',
    shortName: 'Vitamin aqua immunity',
    price: 5.26
}
];

const randFromInterval = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

const slugify = (str) => {
    const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;';
    const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------';
    const p = new RegExp(a.split('').join('|'), 'g');

    return str.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(p, c => b.charAt(a.indexOf(c)))
        .replace(/&/g, '-and-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

module.exports = products.map((product) => ({
    ...product,
    quantity: randFromInterval(0, MAX_PER_SLOT),
    slug: slugify(product.shortName)
}));