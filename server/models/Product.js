const mongoose = require('mongoose');
const schema = mongoose.Schema;

const productModel = new schema({
    name: { type: String },
    slug: { type: String },
    price: { type: Number },
    quantity: { type: Number }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model('products', productModel);
