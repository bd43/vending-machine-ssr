const express = require('express');
const router = express.Router();
const Product = require('../models/Product')

router.get('/', (req, res) => {
    Product.find({}, (err, products) => {
        res.json(products);
    })
});

router.post('/', (req, res) => {
    const doc = Product.findById(req.body.id, (err, product) => {
        if(err)
            res.status(500).send(err);
        else {
            req.product = product;
            req.product.quantity--;
            req.product.save();
            res.json(req.product);
        }
    });
});

router.use('/:id', (req, res, next) => {
    console.log(req.params.id)
    Product.findById(req.params.id, (err, product) => {
        if(err)
            res.status(500).send(err);
        else 
            req.product = product 
            next();
    })
});

router
    .get('/:id', (req, res) => {
        return res.json( req.product )
    })
    .put('/:id', (req, res) => {
        Object.keys(req.body).map(key=>{
            req.product[key] = req.body[key]
        });
        req.product.save()
        res.json(req.product)
    });

module.exports = router;
