process.env.MONGODB_URI = process.env.MONGODB_URI || 'vending-machine';
const productsData = require('./data/products');
const mongoose = require('mongoose');
const { Product } = require('./models');
const connectDB = () => {
    return mongoose.connect(
        `mongodb://localhost:27017/${process.env.MONGODB_URI}`,
        { useNewUrlParser: true, useUnifiedTopology: true }
    )
}

const seedDB = async () => {
    await Product.create(productsData, (err, prods) => {
        if (err) return console.error(err);
        else console.log(`The products were inserted succesfully`);
    });
};

const eraseDB = async () => {
    await Promise.all([
        Product.collection.drop()
    ]);
}

module.exports = { connectDB, seedDB, eraseDB };