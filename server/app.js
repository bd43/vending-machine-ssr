// Always use development mode when no ENV is passed
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.MONGODB_URI = process.env.MONGODB_URI || 'vending-machine'

// Dependencies
const express = require('express');
const path = require('path');
const compression = require('compression');
const bodyParser = require('body-parser');
const next = require("next");
const { connectDB, seedDB, eraseDB } = require("./database");
const eraseDatabaseOnSync = true;

// config
const isDev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 8000;
const app = next({ dev: isDev });

// ROUTES! #magic
const handler = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();
    server.use(compression());
    server.use(bodyParser.json());
    server.use(bodyParser.urlencoded({ extended: true }));

    server.use('/public', express.static(path.join(__dirname, 'public')));
    server.use('/api/products', require('./api/products'));

    server.get('/404', (req, res) => {
      res.status(404);
      return app.render(req, res, '/404');
    });


    server.get('*', (req, res) => {
      return handler(req, res);
    });


    connectDB().then(async () => {
      if (eraseDatabaseOnSync) {
        eraseDB();
        seedDB();
      }
      server.listen(PORT, err => {
        if (err) throw err;
        console.log(
          `> Ready on http://localhost:${PORT} (env: ${process.env.NODE_ENV})`
        );
      });
    });
  })
  .catch(ex => {
    console.error(ex.stack);
    process.exit(1);
  });

process.on('unhandledRejection', (reason, promise) => {
  console.log('Unhandled Rejection:', reason.stack || reason);
});
