module.exports = {
  apps: [
    {
      name: 'vending-machine-ssr',
      script: 'server/app.js',
      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      // args: 'one two',
      instances: 2,
      exec_mode: 'cluster',
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development',
        MONGODB_URI: 'vending-machine',
        ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production',
        MONGODB_URI: 'vending-machine',
        ENV: 'production'
      }
    }
  ]
};
